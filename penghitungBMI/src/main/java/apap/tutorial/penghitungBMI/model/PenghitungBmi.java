package apap.tutorial.penghitungBMI.model;
import java.io.Serializable;

public class PenghitungBmi implements Serializable{
    private Double beratBadan;
    private Double tinggiBadan;
    private String kategori;

    public PenghitungBmi(String beratBadan, String tinggiBadan){
        this.beratBadan = Double.parseDouble((beratBadan));
        this.tinggiBadan = Double.parseDouble((tinggiBadan))/100;
    }

    public String hitungBmi(){
        double hasilBumi = beratBadan / (Math.pow(tinggiBadan, 2));
        if(hasilBumi < 18.5) {
            this.kategori = "Berat badan anda kurang";
        }else if (hasilBumi >= 18.5 && hasilBumi <= 22.9){
            this.kategori = "Berat badan anda normal";
        }else if(hasilBumi > 22.9 && hasilBumi <= 29.9){
            this.kategori = "Berat badan anda berlebih (kecenderungan obesitas)";
        }else{
            this.kategori = "Anda obesitas";
        }

        return String.format("%.2f", hasilBumi);
    }


    public double getWeight() {return this.beratBadan;}
    public double getHeight() {return this.tinggiBadan;}
    public String getKategori() {return kategori;}

}