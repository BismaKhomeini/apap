package apap.tutorial.belajarbelajar.model;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name="course")
public class CourseModel {
    @Id
    @Size(max = 30)
    private String code;

    @NotNull
    @Size(max = 30)
    @Column(name="name_course", nullable = true)
    private String nameCourse;

    @NotNull
    @Size(max = 30)
    @Column(name="description", nullable = true)
    private String description;

    @NotNull
    @Column(name="jumlah_sks", nullable = true)
    private Integer jumlahSks;


    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime tanggalDimulai;


    @NotNull
    @Column(nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
    private LocalDateTime tanggalBerakhir;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<PengajarModel> listPengajar;

    @ManyToMany
    @JoinTable(name="penyelenggara_course", joinColumns = @JoinColumn(name="code"), inverseJoinColumns = @JoinColumn(name="no_penyelenggara"))
    List<PenyelenggaraModel> listPenyelenggara;


    public List<PengajarModel> getListPengajar() {
        return this.listPengajar;
    }

    public void setListPengajar(List<PengajarModel> listPengajar) {
        this.listPengajar = listPengajar;
    }

    public List<PenyelenggaraModel> getListPenyelenggara() {
        return this.listPenyelenggara;
    }

    public void setListPenyelenggara(List<PenyelenggaraModel> listPenyelenggara) {
        this.listPenyelenggara = listPenyelenggara;
    }

    @Override
    public String toString() {
        return "{" +
            " code='" + getCode() + "'" +
            ", nameCourse='" + getNameCourse() + "'" +
            ", description='" + getDescription() + "'" +
            ", jumlahSks='" + getJumlahSks() + "'" +
            ", tanggalDimulai='" + getTanggalDimulai() + "'" +
            ", tanggalBerakhir='" + getTanggalBerakhir() + "'" +
            ", listPengajar='" + getListPengajar() + "'" +
            ", listPenyelenggara='" + getListPenyelenggara() + "'" +
            "}";
    }
    



}
