package apap.tutorial.belajarbelajar.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@JsonIgnoreProperties(value = {"course"}, allowSetters = true)
// @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name="pengajar")
public class PengajarModel implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long noPengajar;

    @NotNull
    @Size(max=30)
    @Column(name="nama_pengajar", nullable = false)
    private String namaPengajar;

    @NotNull
    @Column(name="is_pengajar_universitas", nullable = false)
    private Boolean isPengajarUniversitas;

    @Column(name="gender")
    private Boolean gender;
    

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @JoinColumn(name="code", referencedColumnName = "code", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private CourseModel  course;


    @Override
    public String toString() {
        return "{" +
            " noPengajar='" + getNoPengajar() + "'" +
            ", namaPengajar='" + getNamaPengajar() + "'" +
            ", isPengajarUniversitas='" + this.isPengajarUniversitas + "'" +
            ", course='" + getCourse() + "'" +
            "}";
    }
    
}
