package apap.tutorial.belajarbelajar.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Entity@Data
@Table(name="penyelenggara")
public class PenyelenggaraModel implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long noPenyelenggara;

    @NotNull
    @Size(max=30)
    @Column(name="nama_lembaga", nullable = false)
    private String namaPenyelenggara;

    @NotNull
    @Column(name="jenis_lembaga", nullable = false)
    private Integer jenisLembaga;


    @ManyToMany(mappedBy = "listPenyelenggara")
    List<CourseModel> listCourse;

}
