package apap.tutorial.belajarbelajar.dto;

import lombok.Data;


public class PengajarDto {
    public Long noPengajar;
    public String namaPengajar;
    public Boolean isPengajarUniversitas;

    public PengajarDto() {
    }

    public PengajarDto(Long noPengajar, String namaPengajar, Boolean isPengajarUniversitas) {
        this.noPengajar = noPengajar;
        this.namaPengajar = namaPengajar;
        this.isPengajarUniversitas = isPengajarUniversitas;
    }

    public Long getNoPengajar() {
        return this.noPengajar;
    }

    public void setNoPengajar(Long noPengajar) {
        this.noPengajar = noPengajar;
    }

    public String getNamaPengajar() {
        return this.namaPengajar;
    }

    public void setNamaPengajar(String namaPengajar) {
        this.namaPengajar = namaPengajar;
    }

    public Boolean isIsPengajarUniversitas() {
        return this.isPengajarUniversitas;
    }

    public Boolean getIsPengajarUniversitas() {
        return this.isPengajarUniversitas;
    }

    public void setIsPengajarUniversitas(Boolean isPengajarUniversitas) {
        this.isPengajarUniversitas = isPengajarUniversitas;
    }

    public PengajarDto noPengajar(Long noPengajar) {
        setNoPengajar(noPengajar);
        return this;
    }

    public PengajarDto namaPengajar(String namaPengajar) {
        setNamaPengajar(namaPengajar);
        return this;
    }

    public PengajarDto isPengajarUniversitas(Boolean isPengajarUniversitas) {
        setIsPengajarUniversitas(isPengajarUniversitas);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " noPengajar='" + getNoPengajar() + "'" +
            ", namaPengajar='" + getNamaPengajar() + "'" +
            ", isPengajarUniversitas='" + isIsPengajarUniversitas() + "'" +
            "}";
    }
}