package apap.tutorial.belajarbelajar.dto;

public class GenderizeDto {
    public int count;
    public String gender;
    public String name;
    public double probability;


    public GenderizeDto() {
    }

    public GenderizeDto(int count, String gender, String name, double probability) {
        this.count = count;
        this.gender = gender;
        this.name = name;
        this.probability = probability;
    }

    public int getCount() {
        return this.count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getProbability() {
        return this.probability;
    }

    public void setProbability(double probability) {
        this.probability = probability;
    }

    public GenderizeDto count(int count) {
        setCount(count);
        return this;
    }

    public GenderizeDto gender(String gender) {
        setGender(gender);
        return this;
    }

    public GenderizeDto name(String name) {
        setName(name);
        return this;
    }

    public GenderizeDto probability(double probability) {
        setProbability(probability);
        return this;
    }


    @Override
    public String toString() {
        return "{" +
            " count='" + getCount() + "'" +
            ", gender='" + getGender() + "'" +
            ", name='" + getName() + "'" +
            ", probability='" + getProbability() + "'" +
            "}";
    }

}
