package apap.tutorial.belajarbelajar.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StatusDto{

    @JsonProperty("status")
    public String status;
}