package apap.tutorial.belajarbelajar.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FullStatusDto {

    @JsonProperty("status")
    public String status;

    @JsonProperty("validUntil")
    public String validUntil;

    @JsonProperty("courseLisence")
    public int courseLisence;
}
