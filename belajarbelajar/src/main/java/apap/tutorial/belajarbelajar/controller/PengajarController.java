package apap.tutorial.belajarbelajar.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.service.CourseService;
import apap.tutorial.belajarbelajar.service.PengajarService;

@Controller
public class PengajarController {

    @Qualifier("courseServiceImpl")
    @Autowired
    private CourseService courseService;

    @Qualifier("pengajarServiceImpl")
    @Autowired
    private PengajarService pengajarService;

    @GetMapping("/pengajar/add")
    public String addPengajarFormPage(
        @ModelAttribute PengajarModel pengajar, 
        @RequestParam(value = "code") String code,
        Model model){
        List<CourseModel> lstCourse = courseService.getListCourse();
        model.addAttribute("listCourse", lstCourse);
        model.addAttribute("pengajar", new PengajarModel());
        model.addAttribute("code", code);
        model.addAttribute("page", "pengajar");
        return "form-add-pengajar";   
    }

    @PostMapping("/pengajar/add")
    public String addPengajarSubmitPage(
        @ModelAttribute PengajarModel pengajar,
        @RequestParam(value = "code") String code,
         Model model){
        System.out.println(pengajar);
        pengajarService.addPengajar(pengajar, code);
        model.addAttribute("noPengajar", pengajar.getNoPengajar());
        model.addAttribute("code", code);
        model.addAttribute("page", "pengajar");
        return "add-pengajar";
    }

    @GetMapping("/pengajar/update")
    public String updatePengajarFormPage(
        @RequestParam(value = "noPengajar") String noPengajar,
        @ModelAttribute PengajarModel pengajar, Model model){

        model.addAttribute("noPengajar", noPengajar);
        model.addAttribute("pengajar", new PengajarModel());
        model.addAttribute("page", "pengajar");
        
        return "form-update-pengajar";   
    }

    @PostMapping("/pengajar/update")
    public String updatePengajarSubmitPage(
        @ModelAttribute PengajarModel pengajar, 
        @RequestParam(value = "noPengajar") String noPengajar,
        Model model){

        PengajarModel pengajarNow =  pengajarService.updatePengajar(pengajar, Long.parseLong(noPengajar));
        model.addAttribute("page", "pengajar");

        model.addAttribute("noPengajar", pengajar.getNoPengajar());
        if (pengajarNow != null)
            return "update-pengajar";
        
        model.addAttribute("context", "Fail to update pengajar : course is still running");
        return "error-pages";
    }


    @RequestMapping("/pengajar/delete")
    public String deletePengajarSubmitPage(
        @ModelAttribute CourseModel course,
        Model model){
        
        model.addAttribute("page", "pengajar");
        if(courseService.isClosed(course.getTanggalDimulai(), course.getTanggalBerakhir())){
            for(PengajarModel pengajar : course.getListPengajar()){
                pengajarService.deletePengajarByNoPengajar(pengajar.getNoPengajar());
            }

            model.addAttribute("code", course.getCode());
            return "delete-pengajar";
        }

        return "gagal-delete-pengajar";
    }
}

