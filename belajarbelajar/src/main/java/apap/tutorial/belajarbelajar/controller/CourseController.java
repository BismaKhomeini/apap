package apap.tutorial.belajarbelajar.controller;

import apap.tutorial.belajarbelajar.service.CourseService;
import apap.tutorial.belajarbelajar.service.PengajarService;
import apap.tutorial.belajarbelajar.service.PenyelenggaraService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.engine.AttributeName;
import org.springframework.ui.Model;

import apap.tutorial.belajarbelajar.dto.PengajarDto;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.model.PenyelenggaraModel;
import apap.tutorial.belajarbelajar.service.CourseService;

import java.sql.RowId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class CourseController {

    @Qualifier("courseServiceImpl")
    @Autowired
    private CourseService courseService;


    @Qualifier("penyelenggaraServiceImpl")
    @Autowired
    private PenyelenggaraService penyelenggaraService;


    @Qualifier("pengajarServiceImpl")
    @Autowired
    private PengajarService pengajarService;


    @GetMapping("/course/add")
    public String addCourseFormPage(Model model){

        CourseModel course = new CourseModel();

        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();
        List<PenyelenggaraModel> listPenyelenggaNew = new ArrayList<>();

        // Penyelenggara
        course.setListPenyelenggara(listPenyelenggaNew);
        course.getListPenyelenggara().add(new PenyelenggaraModel());


        model.addAttribute("course", new CourseModel());
        model.addAttribute("listPenyelenggaraExisting",listPenyelenggara);


        // Pengajar
        List<PengajarModel> listNewPengajar = new ArrayList<>();
        course.setListPengajar(listNewPengajar);
        course.getListPengajar().add(new PengajarModel());

        model.addAttribute("page", "course");
        return "form-add-course";
    }

    @PostMapping(value="/course/add", params={"addPenyelenggaraRow"})
    public String addRowsPenyelenggaraMultiple(
        @ModelAttribute CourseModel course,
        Model model){

        if(course.getListPenyelenggara() == null || course.getListPenyelenggara().size() == 0){
            course.setListPenyelenggara(new ArrayList<>());
        }

        course.getListPenyelenggara().add(new PenyelenggaraModel());

        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting",listPenyelenggara);
        model.addAttribute("page", "course");
        return "form-add-course";
    }

    @PostMapping(value="/course/add", params={"addPengajarRow"})
    public String addRowsPengajarMultiple(
        @ModelAttribute CourseModel course,
        Model model){

        if(course.getListPengajar() == null || course.getListPengajar().size() == 0){
            course.setListPengajar(new ArrayList<>());
        }

        course.getListPengajar().add(new PengajarModel());
        model.addAttribute("course", course);
        model.addAttribute("page", "course");
        return "form-add-course";
    }

    @PostMapping(value="/course/add", params={"deletePenyelenggaraRow"})
    public String deleteRowsPenyelenggaraMultiple(
        @ModelAttribute CourseModel course,
        @RequestParam("deletePenyelenggaraRow") Integer row,
        Model model){

        final Integer rowId = Integer.valueOf(row);
        course.getListPenyelenggara().remove(rowId.intValue());

        List<PenyelenggaraModel> listPenyelenggara = penyelenggaraService.getListPenyelenggara();

        model.addAttribute("course", course);
        model.addAttribute("listPenyelenggaraExisting",listPenyelenggara);
        model.addAttribute("page", "course");
        return "form-add-course";
    }


    @PostMapping(value="/course/add", params={"deletePengajarRow"})
    public String deleteRowsPengajaraMultiple(
        @ModelAttribute CourseModel course,
        @RequestParam("deletePengajarRow") Integer row,
        Model model){

        final Integer rowId = Integer.valueOf(row);
        course.getListPengajar().remove(rowId.intValue());

        model.addAttribute("course", course);
        model.addAttribute("page", "course");
        return "form-add-course";
    }


    @PostMapping(value="/course/add", params = {"save"})
    public String addCourseSubmitPage(@ModelAttribute CourseModel course, Model model){
        if(course.getListPenyelenggara() == null){
            course.setListPenyelenggara(new ArrayList<>());
        }

        List<PengajarModel> lstPengajar = course.getListPengajar();

        if(course.getListPengajar() == null){
            lstPengajar = new ArrayList<>();
        }

        course.setListPengajar(null);
        
        boolean isSaved = courseService.addCourse(course);

        if(isSaved){
            for(PengajarModel x : lstPengajar){
                pengajarService.addPengajar(x, course.getCode());
            }    
    
            model.addAttribute("code", course.getCode());
            model.addAttribute("page", "course");
            return "add-course";  
        }else{
            model.addAttribute("context", "fail add course");
            model.addAttribute("page", "course");
            return "error-pages";  
        }

         
    }

    @GetMapping("/course/viewall")
    public String listCourse(
        @RequestParam(value="sortBy", required = false) String sortBy,
        Model model){
        List<CourseModel> listCourse = courseService.getListCourse();

        if(sortBy != null){
            listCourse = courseService.getListCourseOrderByName();            
        }

        model.addAttribute("listCourse", listCourse);
        model.addAttribute("page", "course");
        return "viewall-course";
    }

    @GetMapping("/course/view")
    public String viewDetailCoursePage(@RequestParam(value="code") String code, Model model){
        CourseModel course  = courseService.getCourseByCodeCourse(code);
        List<PengajarModel> listPengajar = pengajarService.listCoursePengajar(code);
        List<PenyelenggaraModel> listPenyelenggara = course.getListPenyelenggara();

        List<PengajarDto> pengajarDto = new ArrayList<>();

        for(PengajarModel x : listPengajar){
            pengajarDto.add(new PengajarDto(x.getNoPengajar(), x.getNamaPengajar(), x.getIsPengajarUniversitas()));
        }

        for(PengajarDto x : pengajarDto){
            System.out.println(x);
        }

        model.addAttribute("listPengajar", pengajarDto);
        model.addAttribute("listPenyelenggara", listPenyelenggara);
        model.addAttribute("course", course);
        model.addAttribute("page", "course");
        return "view-course";
    }


    @GetMapping("/course/view-query")
    public String viewDetailCoursePageQuery(@RequestParam(value="code") String code, Model model){
        CourseModel course = courseService.getCourseByCodeCourse(code);
        List<PengajarModel> listPengajar = course.getListPengajar();

        model.addAttribute("listPengajar", listPengajar);
        model.addAttribute("course", course);
        model.addAttribute("page", "course");

        return "view-course";
    }

    @GetMapping("/course/update/{code}")
    public String updateCourseFormPage(@PathVariable String code, Model model){
        CourseModel course = courseService.getCourseByCodeCourse(code);
        model.addAttribute("course", course);
        model.addAttribute("page", "course");

        return "form-update-course";
    }

    @PostMapping("/course/update")
    public String updateCourse(@PathVariable String code, Model model){
        CourseModel course = courseService.getCourseByCodeCourse(code);
        model.addAttribute("code", course.getCode());
        model.addAttribute("page", "course");
        return "form-update-course";
    }

    @RequestMapping("/course/delete")
    public String delteCourse(
        @RequestParam(value = "code") String code,
        Model model
    ){
        CourseModel course = courseService.deleteCourse(code);
        model.addAttribute("page", "course");
        
        if(course != null){
            model.addAttribute("code", course.getCode());
            return "delete-course";
        }else{
            model.addAttribute("code", "Nill");
            model.addAttribute("context", "Cannot delete this course");
            return "error-pages";
        }
    }

    
}
