package apap.tutorial.belajarbelajar.controller;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import apap.tutorial.belajarbelajar.dto.FullStatusDto;
import apap.tutorial.belajarbelajar.dto.StatusDto;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.service.CourseRestService;
import apap.tutorial.belajarbelajar.service.CourseService;

@RestController
@RequestMapping("/api/v1")
public class CourseRestController {

    @Autowired
    private CourseRestService courseRestService;

    @Autowired
    private CourseService courseService;

    @PostMapping(value = "/course/add")
    private CourseModel createCouse(
        @Valid @RequestBody CourseModel course,
        BindingResult bindingResult
    ){
        if(bindingResult.hasFieldErrors()){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Request body has invalid type or missing field"
            );
        }else{
            return courseRestService.createOne(course);
        }
    }

    @GetMapping(value = "/course/{code}")
    private CourseModel retriveCourse(@PathVariable("code") String code){
        try{
            CourseModel course = courseRestService.getCourseByCode(code);
            return course;
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Code course " + code + " not found"
            );
        }
    }

    @GetMapping(value="/course/list-course")
    private List<CourseModel> retrieveListCourse(){
        return courseRestService.retrieveListCourse();
    }

    @DeleteMapping(value="/course/{code}")
    private ResponseEntity deleteCourse(
        @PathVariable("code") String code
    ){
        try{
            courseRestService.deleteCourse(code);
            return ResponseEntity.ok("Course with code " + code + " has been deleted succesfully");
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND,
                "Code course "+code+" not found"
            );
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Course is still open for pengajar"
            );
        }
    }

    @PutMapping(value = "/course/{code}")
    private CourseModel updateCourse(
        @PathVariable("code") String code,
        @RequestBody CourseModel course
    ){
        try{
            return courseRestService.updateCourse(code, course);
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Code course " + code + " not found"
            );
        }
    }

    @GetMapping(value = "/couse/status")
    private StatusDto getStatus(){
        try{
            return courseRestService.getStatus("APAP");
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, ""
            );
        }
    }


    @PostMapping(value = "/course/full-status")
    private FullStatusDto getFullStatus(){
        try{
            return courseRestService.postStatus();
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, ""
            );
        }
    }
    
}
