package apap.tutorial.belajarbelajar.controller;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.service.CourseRestService;
import apap.tutorial.belajarbelajar.service.CourseService;
import apap.tutorial.belajarbelajar.service.PengajarRestService;
import apap.tutorial.belajarbelajar.service.PengajarService;


@RestController
@RequestMapping("/api/v1")
public class PengajarRestController {

    @Autowired
    PengajarRestService pengajarRestService;

    @Autowired
    CourseRestService courseRestService;
    

    @PostMapping(value = "/pengajar")
    public PengajarModel createNewPengajar(
        @Valid @RequestBody PengajarModel pengajar,
        BindingResult bindingResult
    ){

        if(bindingResult.hasFieldErrors()){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST, "Request body has invalid type or missing field"
            );
        }else{
            return pengajarRestService.addPengajar(pengajar,pengajar.getCourse().getCode());
        }
    }


    @GetMapping(value = "/pengajar/{noPengajar}")
    private PengajarModel retrivePengajar(@PathVariable("noPengajar") String id){
        try{
            PengajarModel pengajar = pengajarRestService.findOneByNoPengajar(Long.parseLong(id));
            // System.out.println(pengajar);
            return pengajar;
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Pengajar with id  " + id + " not found"
            );
        }
    }

    @GetMapping(value = "/pengajar/jenis-kelamin/{noPengajar}")
    private PengajarModel predictGender(@PathVariable("noPengajar") String id){
        try{
            PengajarModel pengajar = pengajarRestService.predictGender(Long.parseLong(id));
            // System.out.println(pengajar);
            return pengajar;
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Pengajar with id  " + id + " not found"
            );
        }
    }

    @GetMapping(value="/pengajar/list-course")
    private List<PengajarModel> retrieveListPengajar(){
        return pengajarRestService.listAllPengajar();
    }

    @DeleteMapping(value="/pengajar/{noPengajar}")
    private ResponseEntity deleteCourse(
        @PathVariable("noPengajar") String id
    ){
        try{
            pengajarRestService.deletePengajarByNoPengajar(Long.parseLong(id));
            return ResponseEntity.ok("Pengajar with id " + id + " has been deleted");
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Pengajar with id  " + id + " not found"
            );
        }catch(UnsupportedOperationException e){
            throw new ResponseStatusException(
                HttpStatus.BAD_REQUEST,
                "Course is still open for pengajar"
            );
        }
    }
    
    @PutMapping(value = "/pengajar/{noPengajar}")
    private PengajarModel updateCourse(
        @PathVariable("noPengajar") String id,
        @RequestBody PengajarModel pengajar,
        BindingResult bindingResult
    ){
        try{
            return pengajarRestService.updatePengajar(pengajar, Long.parseLong(id));
        }catch(NoSuchElementException e){
            throw new ResponseStatusException(
                HttpStatus.NOT_FOUND, "Pengajar with id  " + id + " not found"
            );
        }
    }

    @GetMapping("/pengajar/course/{code}")
    private List<PengajarModel> getPengajarCourse(
        @PathVariable("code") String code
    ){

        List<PengajarModel> pengajar = pengajarRestService.findPengajarCourse(code);
        return pengajar;
    }
}
