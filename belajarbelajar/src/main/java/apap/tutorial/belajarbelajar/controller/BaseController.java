package apap.tutorial.belajarbelajar.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BaseController {
    @GetMapping("/")
    private String Home(
        Model ctx
    ){ 
        ctx.addAttribute("page", "home");
        return "home";
    }

    
}
