package apap.tutorial.belajarbelajar.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import apap.tutorial.belajarbelajar.model.PengajarModel;
import org.springframework.data.repository.query.Param;


@Repository
public interface PengajarDb extends JpaRepository<PengajarModel, Long>{

        @Query("select p from PengajarModel p WHERE p.noPengajar = :noPengajar")
        Optional<PengajarModel> findByNoPengajar(@Param("noPengajar") Integer noPengajar);

        @Modifying
        @Query("update PengajarModel p set p.namaPengajar = :namaPengajar WHERE p.noPengajar = :noPengajar")
        void updateNamaPengajar(
                @Param("noPengajar") Long noPengajar,
                 @Param("namaPengajar") String namaPengajar);

        @Modifying
        @Query("update PengajarModel p set p.isPengajarUniversitas = :isPengajarUniversitas WHERE p.noPengajar = :noPengajar")
        void updateIsPengajarUniversitas(
                @Param("noPengajar") Long noPengajar,
                @Param("isPengajarUniversitas") Boolean isPengajarUniversitas);        

        @Modifying
        @Query("update PengajarModel p set p.gender = :gender WHERE p.noPengajar = :noPengajar")
        void updateGender(
                @Param("noPengajar") Long noPengajar,
                @Param("gender") Boolean gender);  
        
        @Modifying
        @Transactional
        @Query(
                value = "update pengajar p set p.code = :code WHERE p.no_pengajar = :noPengajar",
                nativeQuery = true
        )
        void updatePengajarCourse(
                @Param("noPengajar") Long noPengajar,
                @Param("code") String code); 

        @Query(
                value = "SELECT * from pengajar p WHERE code = :code ORDER BY nama_pengajar ;",
                nativeQuery = true
        )
        List<PengajarModel> findPengajarFromCourse(
                @Param("code") String code
        );
                 
}
