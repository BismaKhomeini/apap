package apap.tutorial.belajarbelajar.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.query.Param;

import apap.tutorial.belajarbelajar.model.CourseModel;


@Repository
public interface CourseDb extends JpaRepository<CourseModel, String> {
    Optional<CourseModel> findByCode(String code);

    @Query("select c from CourseModel c WHERE c.code = :code")
    Optional<CourseModel> findCodeUsingQuery(@Param("code") String code);

    @Query("select c from CourseModel as c order by c.nameCourse DESC")
    List<CourseModel> findAllOrderByName();
}
