package apap.tutorial.belajarbelajar.service;

import java.util.List;

import apap.tutorial.belajarbelajar.dto.FullStatusDto;
import apap.tutorial.belajarbelajar.dto.StatusDto;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.rest.CourseDetail;
import reactor.core.publisher.Mono;

public interface CourseRestService {
    CourseModel createOne(CourseModel course);
    List<CourseModel> retrieveListCourse();
    CourseModel getCourseByCode(String code);
    CourseModel updateCourse(String code, CourseModel courseUpdate);
    void deleteCourse(String code);
    StatusDto getStatus(String code);
    FullStatusDto postStatus();    
}
