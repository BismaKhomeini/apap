package apap.tutorial.belajarbelajar.service;

import java.util.List;
import apap.tutorial.belajarbelajar.model.PengajarModel;

public interface PengajarService {
    void addPengajar(PengajarModel pengajar, String code);
    PengajarModel updatePengajar(PengajarModel dataPengajarModel, Long noPengajar);
    PengajarModel deletePengajarByNoPengajar(Long noPengajar);
    PengajarModel findOneByNoPengajar(Long noPengajar);
    List<PengajarModel> listAllPengajar();
    List<PengajarModel> listCoursePengajar(String courseCode);
}
