package apap.tutorial.belajarbelajar.service;
import  apap.tutorial.belajarbelajar.model.CourseModel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;


public interface CourseService {
    boolean addCourse(CourseModel courseModel);
    List<CourseModel> getListCourse();
    List<CourseModel> getListCourseOrderByName(); 
    CourseModel getCourseByCodeCourse(String code);
    CourseModel getCourseByCodeCourseQuery(String code);
    CourseModel updateCourse(CourseModel course);
    CourseModel deleteCourse(String code);
    boolean isClosed(LocalDateTime tanggalMulai, LocalDateTime tanggalBerakhir);
}
