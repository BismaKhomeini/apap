package apap.tutorial.belajarbelajar.service;


import java.lang.System.Logger;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.constraints.Null;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

import apap.tutorial.belajarbelajar.dto.GenderizeDto;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.repository.PengajarDb;

import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  

@Service
@Transactional
public class PengajarRestServiceImpl implements PengajarRestService{
    @Autowired
    PengajarDb pengajarDb;

    @Autowired
    CourseService courseService;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public PengajarModel findOneByNoPengajar(Long noPengajar){
        return pengajarDb.findById(noPengajar).get();
    }

    @Override
    public List <PengajarModel> findPengajarCourse(String code){
        return pengajarDb.findPengajarFromCourse(code);
    }

    @Override
    public PengajarModel predictGender(Long noPengajar){

        PengajarModel pengajar =  findOneByNoPengajar(noPengajar);

        String firstName = pengajar.getNamaPengajar();

        GenderizeDto genderizeDto = restTemplate.getForObject("https://api.genderize.io/?name=" + firstName, GenderizeDto.class);

        if(genderizeDto.gender.equals("female")){
            pengajar.setGender(true);
        }else{
            pengajar.setGender(false);
        }

        pengajarDb.updateGender(pengajar.getNoPengajar(), pengajar.getGender());

        return pengajar;
    }


    @Override
    public PengajarModel addPengajar(PengajarModel pengajar, String code){

      
        CourseModel course = courseService.getCourseByCodeCourse(code);

        pengajar.setCourse(course);
        
        return pengajarDb.save(pengajar);
    }

    @Override
    public List<PengajarModel> listAllPengajar(){
        return pengajarDb.findAll();
    }

    @Override
    public List<PengajarModel> listCoursePengajar(String courseCode){
        List<PengajarModel> lst = this.listAllPengajar();

        List<PengajarModel> filterLst = new ArrayList<>();

        for(PengajarModel x : lst){
            if(x.getCourse().getCode().equals(courseCode))
                filterLst.add(x);
        }

        return filterLst;
    }


    @Override
    public PengajarModel updatePengajar(PengajarModel dataPengajar, Long noPengajar){

        PengajarModel pengajar = findOneByNoPengajar(noPengajar);

        System.out.println(pengajar.getNamaPengajar());
        // LocalDateTime courseBegin = pengajar.getCourse().getTanggalDimulai();
        LocalDateTime courseEnd = pengajar.getCourse().getTanggalBerakhir();

        LocalDateTime ldt =  LocalDateTime.now();

        if(!ldt.isAfter(courseEnd))
            return null;

        if(pengajar != null){   
            pengajarDb.updateNamaPengajar(noPengajar,  dataPengajar.getNamaPengajar());
            pengajarDb.updateIsPengajarUniversitas(noPengajar, dataPengajar.getIsPengajarUniversitas());
            pengajarDb.updatePengajarCourse(noPengajar, dataPengajar.getCourse().getCode());

            PengajarModel pengajarUpdated = findOneByNoPengajar(noPengajar);
            return pengajarUpdated;
        }

        return null;
    }

    @Override
     public PengajarModel deletePengajarByNoPengajar(Long noPengajar){


        PengajarModel pengajar = pengajarDb.getReferenceById(noPengajar);

        LocalDateTime courseEnd = pengajar.getCourse().getTanggalBerakhir();

        LocalDateTime ldt =  LocalDateTime.now();

        if(!ldt.isAfter(courseEnd))
            return null;

        if(pengajar != null){   
            pengajarDb.deleteById(noPengajar);
            return pengajar;
        }
        return null;

    }


    public String getFirstName(String full_name) {
      int last_space_index = full_name.lastIndexOf(' ');
      if (last_space_index == -1) // single name
          return full_name;
      else
          return full_name.substring(0, last_space_index);
    }
}


