package apap.tutorial.belajarbelajar.service;

import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.repository.CourseDb;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import java.util.List;
import javax.transaction.Transactional;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseDb courseDb;
    
    @Override
    public boolean addCourse(CourseModel course){
        CourseModel x = courseDb.save(course);

        if(x != null)
            return true;
        return false;
    }

    @Override
    public List<CourseModel> getListCourse(){
        return courseDb.findAll();
    }

    @Override
    public List<CourseModel> getListCourseOrderByName(){
        return courseDb.findAllOrderByName();
    }

    @Override
    public CourseModel getCourseByCodeCourse(String code){
        Optional<CourseModel> course = courseDb.findByCode(code);

        if(course.isPresent()){
            return course.get();
        }

        return null;
    }

    @Override
   public CourseModel getCourseByCodeCourseQuery(String code){
        Optional<CourseModel> course = courseDb.findCodeUsingQuery(code);

        if(course.isPresent()){
            return course.get();
        }

        return null;
    }

    @Override
   public CourseModel updateCourse(CourseModel course){
        courseDb.save(course);
        return course;
    }


    @Override
    public CourseModel deleteCourse(String code){
        
        Optional<CourseModel> course = courseDb.findCodeUsingQuery(code);

        if(course.isPresent()){
            CourseModel thisCourse = course.get();
            // LocalDateTime courseBegin = pengajar.getCourse().getTanggalDimulai();

            if (thisCourse.getListPengajar().size() != 0){
                return null;
            }       

            courseDb.deleteById(code);

            return thisCourse;
        }

        return null;
    }

    @Override
    public boolean isClosed(LocalDateTime tanggalMulai, LocalDateTime tanggalBerakhir){
        LocalDateTime ldt =  LocalDateTime.now();
        if(ldt.isBefore(tanggalMulai) || ldt.isAfter(tanggalBerakhir))
            return true;
        return false ;
    }
}


