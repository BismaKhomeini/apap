package apap.tutorial.belajarbelajar.service;

import java.lang.System.Logger;
import java.util.Optional;

import javax.transaction.Transactional;
import javax.validation.constraints.Null;

import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.model.PengajarModel;
import apap.tutorial.belajarbelajar.repository.PengajarDb;

import java.time.LocalDateTime;  
import java.time.format.DateTimeFormatter;  

@Service
@Transactional
public class PengajarServiceImpl implements PengajarService{
    @Autowired
    PengajarDb pengajarDb;

    @Autowired
    CourseService courseService;

    @Override
    public PengajarModel findOneByNoPengajar(Long noPengajar){
        return pengajarDb.getReferenceById(noPengajar);
    }

    @Override
    public void addPengajar(PengajarModel pengajar, String code){

        CourseModel course = courseService.getCourseByCodeCourse(code);

        pengajar.setCourse(course);
        
        pengajarDb.save(pengajar);
    }

    @Override
    public List<PengajarModel> listAllPengajar(){
        return pengajarDb.findAll();
    }

    @Override
    public List<PengajarModel> listCoursePengajar(String courseCode){
        List<PengajarModel> lst = this.listAllPengajar();

        List<PengajarModel> filterLst = new ArrayList<>();


        for(PengajarModel x : lst){
            if(x.getCourse().getCode().equals(courseCode))
                filterLst.add(x);
        }

        return filterLst;
    }


    @Override
    public PengajarModel updatePengajar(PengajarModel dataPengajar, Long noPengajar){

        PengajarModel pengajar = pengajarDb.getReferenceById(noPengajar);

        // LocalDateTime courseBegin = pengajar.getCourse().getTanggalDimulai();
        LocalDateTime courseEnd = pengajar.getCourse().getTanggalBerakhir();

        LocalDateTime ldt =  LocalDateTime.now();

        if(!ldt.isAfter(courseEnd))
            return null;

        if(pengajar != null){   
            pengajarDb.updateNamaPengajar(noPengajar,  dataPengajar.getNamaPengajar());
            pengajarDb.updateIsPengajarUniversitas(noPengajar, dataPengajar.getIsPengajarUniversitas());
            return pengajar;
        }

        return null;
    }

    @Override
     public PengajarModel deletePengajarByNoPengajar(Long noPengajar){


        PengajarModel pengajar = pengajarDb.getReferenceById(noPengajar);

        LocalDateTime courseEnd = pengajar.getCourse().getTanggalBerakhir();

        LocalDateTime ldt =  LocalDateTime.now();

        if(!ldt.isAfter(courseEnd))
            return null;

        if(pengajar != null){   
            pengajarDb.deleteById(noPengajar);
            return pengajar;
        }
        return null;

    }
}
