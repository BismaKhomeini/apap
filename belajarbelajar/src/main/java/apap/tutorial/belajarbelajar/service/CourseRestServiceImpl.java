package apap.tutorial.belajarbelajar.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.boot.autoconfigure.web.*;
import org.springframework.boot.autoconfigure.web.reactive.function.client.WebClientAutoConfiguration;
import org.springframework.boot.web.reactive.function.client.WebClientCustomizer;

import apap.tutorial.belajarbelajar.dto.FullStatusDto;
import apap.tutorial.belajarbelajar.dto.GenderizeDto;
import apap.tutorial.belajarbelajar.dto.StatusDto;
import apap.tutorial.belajarbelajar.model.CourseModel;
import apap.tutorial.belajarbelajar.repository.CourseDb;
import apap.tutorial.belajarbelajar.rest.CourseDetail;
import ch.qos.logback.core.status.Status;
import reactor.core.publisher.Mono;
import org.springframework.boot.*;



@Service
@Transactional
public class CourseRestServiceImpl implements CourseRestService {


    final public static String courseUrl = "https://f9dd2e59-4aed-484c-ace7-c6e71a1f31c3.mock.pstmn.io";

    @Autowired
    private CourseDb courseDb;


    @Autowired
    private RestTemplate restTemplate;

    @Override
    public CourseModel createOne(CourseModel course){
        return courseDb.save(course);
    }

    @Override
    public List<CourseModel> retrieveListCourse(){
        return courseDb.findAll();
    }
    @Override
    public CourseModel getCourseByCode(String code){

        

        Optional<CourseModel> course = courseDb.findByCode(code);
        if(course.isPresent()){
            return course.get();
        }else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public CourseModel updateCourse(String code, CourseModel courseUpdate){
        CourseModel course = getCourseByCode(code);
        course.setNameCourse(courseUpdate.getNameCourse());
        course.setDescription(courseUpdate.getDescription());
        course.setJumlahSks(courseUpdate.getJumlahSks());
        course.setTanggalDimulai(courseUpdate.getTanggalDimulai());
        course.setTanggalBerakhir(courseUpdate.getTanggalBerakhir());
        return courseDb.save(course);
    }

    @Override
    public void deleteCourse(String code){
        CourseModel course = getCourseByCode(code);
        if(isClosed(course.getTanggalDimulai(), course.getTanggalBerakhir())){
            courseDb.delete(course);
        }else{
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public StatusDto getStatus(String code) {
        StatusDto statusDto = restTemplate.getForObject( courseUrl +"/rest/course/APAP/Status", StatusDto.class);
        
        return statusDto;
    }

    @Override
    public FullStatusDto postStatus(){

        FullStatusDto fullStatusDto = restTemplate.getForObject(courseUrl+"/rest/course/APAP/Status", FullStatusDto.class);
        return fullStatusDto;
    }

    public boolean isClosed(LocalDateTime tanggalDimulai, LocalDateTime tanggalBerakhir){
        LocalDateTime now = LocalDateTime.now();

        if(now.isBefore(tanggalDimulai) || now.isAfter(tanggalBerakhir))
            return true;
        return false;
    }
}
